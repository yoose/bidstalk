# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bidstalk/version'

Gem::Specification.new do |spec|
  spec.name          = 'bidstalk'
  spec.version       = Bidstalk::VERSION
  spec.authors       = ['IndexMedia']
  spec.email         = ['tam@indexmedia.com']
  spec.summary       = %q{A Bidstalk API Client.}
  spec.description   = %q{A Bidstalk API Client.}
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_dependency('railties', '>= 3.2.6', '< 5')
  spec.add_dependency 'faraday', '~> 0.9.0'
  spec.add_dependency 'json'

  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec-expectations'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'factory_girl'
  spec.add_development_dependency 'fuubar'
end
