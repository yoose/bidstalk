require 'spec_helper'

describe 'list' do
  context 'with valid list' do
    it 'return list with size > 0' do
      @client = Bidstalk::MetaData::Client.new('age')
      expect(@client.list.size).to be > 0
    end
  end
end