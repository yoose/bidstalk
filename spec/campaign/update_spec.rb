require 'campaign/spec_helper'

describe 'update' do
  include_context 'shared_campaign'
  include_context 'shared_new_campaign'

  context 'with valid updated campaign' do
    it 'returns campaign with new value' do
      @campaign = @client.update(
          title: 'Test update campaign',
          date_range: '2015-03-30 08:05:28|2015-04-07 08:05:2',
          max_bid: 0.1,
          campaign_id: @campaign[:campaign_id],
          timezone: 11
      )
      expect(@campaign).to include(campaign_title: 'Test update campaign')
      expect(@campaign).to include(max_bid: 0.1)
    end
  end

  context 'with invalid campaign_id' do
    it 'returns InvalidArgumentError' do
      campaign = {
          title: 'Test update campaign',
          date_range: '2015-03-30 08:05:28|2015-04-07 08:05:2',
          max_bid: 0.1,
          campaign_id: 1,
          timezone: 11
      }
      expect { @client.update campaign }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end

  context 'with invalid campaign title' do
    it 'returns InvalidArgumentError' do
      campaign = {
          title: 'Test update campaign:error',
          date_range: '2015-03-30 08:05:28|2015-04-07 08:05:2',
          max_bid: 0.1,
          campaign_id: @campaign[:campaign_id],
          timezone: 11
      }
      expect { @client.update campaign }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end
end