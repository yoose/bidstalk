require 'spec_helper'

shared_context 'shared_campaign' do
  before(:all) do
    @client = Bidstalk::Campaign::Client.new
  end

  after(:all) do
    unless @campaign.nil?
      @client.delete @campaign[:campaign_id]
    end
  end
end

shared_context 'shared_new_campaign' do
  before(:all) do
    campaign = {
      title: "Test Campaign api #{Time.now.strftime('%h-%d %H %M')}",
      date_range: '2015-03-29 08:05:28|2015-04-05 08:05:2',
      max_bid: 0.2,
      ad_domain: 'http://yoose.com',
      daily_budget: 25,
      total_budget: 26,
      timezone: 11,
      campaign_type: 1,
      creative_type: 1,
      target_countries: 210,
      target_states: 'all',
      target_carriers: 'all',
      target_connection_type: 'all',
      target_manufacturers: 'all',
      target_os: 'all',
      target_os_version: 'all',
      target_devices: 'all',
      target_categories: 'all',
      target_ages: 'all',
      target_gender: 'all',
      target_interests: 'all',
      target_exchanges: 13,
      conversion_type: 'none',
      market_install_url: 'http://yoose.com',
      conversion_target_cpi: 'none'
    }
    @campaign = @client.create campaign
  end
end