require 'campaign/spec_helper'

describe 'create' do
  include_context 'shared_campaign'
  include_context 'shared_new_campaign'

  context 'with valid campaign' do
    it 'return hash with :campaign_id' do
      expect(@campaign).to include(:campaign_id)
    end
  end

  context 'with invalid title' do
    it 'return InvalidArgumentError' do
      campaign = {
          title: 'Test Campaign api:error',
          date_range: '2015-03-29 08:05:28|2015-04-05 08:05:2',
          max_bid: 0.2,
          ad_domain: 'http://yoose.com',
          daily_budget: 25,
          total_budget: 26,
          timezone: 11,
          campaign_type: 1,
          creative_type: 1,
          target_countries: 210,
          target_states: 'all',
          target_carriers: 'all',
          target_connection_type: 'all',
          target_manufacturers: 'all',
          target_os: 'all',
          target_os_version: 'all',
          target_devices: 'all',
          target_categories: 'all',
          target_ages: 'all',
          target_gender: 'all',
          target_interests: 'all',
          target_exchanges: 13,
          conversion_type: 'none',
          market_install_url: 'http://yoose.com',
          conversion_target_cpi: 'none'
      }
      expect { @client.create campaign }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end

  context 'with invalid required fields' do
    it 'return InvalidArgumentError' do
      campaign = {
          title: 'Test Campaign api:error',
          date_range: '2015-03-29 08:05:28|2015-04-05 08:05:2',
          max_bid: 0.2,
          ad_domain: 'http://yoose.com',
          daily_budget: 25,
          total_budget: 26,
          timezone: 11,
          campaign_type: 1,
          creative_type: 1,
          target_countries: 210,
          target_gender: 'all',
          target_interests: 'all',
          target_exchanges: 13,
          conversion_type: 'none',
          market_install_url: 'http://yoose.com',
          conversion_target_cpi: 'none'
      }
      expect { @client.create campaign }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end
end