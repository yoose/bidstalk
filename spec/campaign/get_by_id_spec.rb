require 'campaign/spec_helper'

describe 'get_by_id' do
  include_context 'shared_campaign'

  context 'with valid campaign id' do
    it 'return campaign with campaign_id equal to the given one' do
      campaign_id = 7797
      @campaign = @client.get_by_id campaign_id
      expect(@campaign).to include(:campaign_id => campaign_id)
    end
  end

  context 'with invalid campaign id' do
    it 'return InvalidArgumentError' do
      campaign_id = 1
      expect { @client.get_by_id campaign_id }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end
end