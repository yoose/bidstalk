require 'spec_helper'

shared_context 'shared_creative' do
  before(:all) do
    @client = Bidstalk::Creative::Client.new
    @creative = nil
  end

  after(:all) do
    unless @creative.nil?
      @client.delete @creative[:creative_id]
    end
  end
end

shared_context 'shared_new_creative' do
  before(:all) do
    creative = {
      campaign_id: 7900,
      creative_title: 'Test creative 2',
      creative_type: 1,
      creative_img_url: 'http://bidstalkcreatives.s3.amazonaws.com/9290e12042de966ab133dcd1254360a9.png',
      creative_target_url: 'http://test.com',
      creative_img_mime: 'image/*'
    }
    @creative = @client.create creative
  end
end