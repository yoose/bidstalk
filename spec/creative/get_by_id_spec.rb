require 'creative/spec_helper'

describe 'get_by_id' do
  include_context 'shared_creative'

  context 'with valid creative id' do
    it 'return creative with creative_id equal to the given one' do
      creative_id = 86470
      @creative = @client.get_by_id creative_id
      expect(@creative).to include(:creative_id => creative_id)
    end
  end

  context 'with invalid creative id' do
    it 'return InvalidArgumentError' do
      creative_id = 1
      expect { @client.get_by_id creative_id }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end
end