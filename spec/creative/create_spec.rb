require 'creative/spec_helper'

describe 'create' do
  include_context 'shared_creative'
  include_context 'shared_new_creative'

  context 'with valid creative' do
    it 'returns hash with :creative_id' do
      expect(@creative).to include(:creative_id)
    end
  end

  context 'with invalid campaign_id' do
    it 'return InvalidArgumentError' do
      creative = {
          campaign_id: 1,
          creative_title: 'Test creative 2',
          creative_type: 1,
          creative_img_url: 'http://bidstalkcreatives.s3.amazonaws.com/9290e12042de966ab133dcd1254360a9.png',
          creative_target_url: 'http://test.com',
          creative_img_mime: 'image/*'
      }
      expect { @client.create creative }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end

  context 'with invalid creative_title' do
    it 'return InvalidArgumentError' do
      creative = {
          campaign_id: 7900,
          creative_title: 'Test creative 2:error',
          creative_type: 1,
          creative_img_url: 'http://bidstalkcreatives.s3.amazonaws.com/9290e12042de966ab133dcd1254360a9.png',
          creative_target_url: 'http://test.com',
          creative_img_mime: 'image/*'
      }
      expect { @client.create creative }.to raise_error(Bidstalk::InvalidArgumentError)
    end
  end
end