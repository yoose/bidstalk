require 'creative/spec_helper'

describe 'update' do
  include_context 'shared_creative'
  include_context 'shared_new_creative'

  context 'with valid updated creative' do
    it 'returns creative with new value' do
      @creative = @client.update(
          creative_title: 'Test update creative 2',
          creative_img_url: 'http://bidstalkcreatives.s3.amazonaws.com/f7ed9739a40b850701803aabcde979b9.png',
          creative_target_url: 'http://test2.com',
          creative_id: @creative[:creative_id]
      )
      expect(@creative).to include(creative_title: 'Test update creative 2')
      expect(@creative).to include(creative_img_url: 'http://bidstalkcreatives.s3.amazonaws.com/f7ed9739a40b850701803aabcde979b9.png')
      expect(@creative).to include(creative_target_url: 'http://test2.com')
    end
  end
end