require 'bundler/setup'
require 'pry-byebug'

require 'bidstalk'

Bundler.setup


RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

Bidstalk.configure do |config|
  config.api_key = '29e8021bc18f9c9bd5845e492f9d3307'
  config.username = 'it@yoose.com'
end
