require 'bidstalk/client'

module Bidstalk
  module Creative
    class Client < Bidstalk::Client

      def initialize(campaign_id)
        @campaign_id = campaign_id
        @name = 'images'
        @endpoint = "/advertiser/images"
      end

      # Create creative on bidstalk
      # @param [Hash] creative: creative detail
      # @return [Hash] Creative detail with creative_id if successful
      def create(creative)
        item post({body: creative, path: '/create', content_type: APPLICATION_JSON})
      end

      # Get creative from bidstalk by id
      # @param [Integer] creative_id
      # @return [Hash] Creative detail if successful
      def get_by_id(creative_id)
        item get(creative_id: creative_id)
      end

      # Update creative on bidstalk
      # @param [Hash] creative: creative detail to update to bidstalk
      # @return [Hash] Creative detail if successful
      def update(creative)
        item put(body: creative, content_type: APPLICATION_FORM_URLENCODED)
      end

      # Pause creative on bidstalk
      # @param [Integer] creative_id
      # @return [Hash] {creative_id: id, creative_title: title, creative_status: status}
      def pause(creative_id)
        item put(path: '/pause', body: {creative_id: creative_id}, content_type: APPLICATION_FORM_URLENCODED)
      end

      # Resume creative on bidstalk
      # @param [Integer] creative_id
      # @return [Hash] {creative_id: id, creative_title: title, creative_status: status}
      def resume(creative_id)
        item put(path: '/resume', body: {creative_id: creative_id}, content_type: APPLICATION_FORM_URLENCODED)
      end

      # Delete a creative
      # @param [Integer] creative_id
      # @return [Hash] {creative_id: id, creative_title: title, creative_status: status}
      def delete(creative_id)
        item put(path: '/delete', body: {creative_id: creative_id}, content_type: APPLICATION_FORM_URLENCODED)
      end
    end
  end
end