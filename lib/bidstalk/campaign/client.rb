require 'bidstalk/client'

module Bidstalk
  module Campaign
    class Client < Bidstalk::Client
      ENDPOINT = '/advertiser/campaigns'

      def initialize
        @name = 'campaign'
      end

      # Create a bidstalk campaign
      # @param [Hash] campaign: bidstalk campaign detail
      # @return [Hash] Campaign detail with campaign_id if successful
      def create(campaign)
        item post(body: campaign, path: "#{ENDPOINT}/create", content_type: APPLICATION_JSON)
      end

      # Get campaign detail by id
      # @param [Integer] campaign_id
      # @return [Hash] Campaign detail if successful
      def get_by_id(campaign_id)
        item post(path: "#{ENDPOINT}/show/#{campaign_id}", content_type: APPLICATION_JSON)
      end

      # Update campaign on bidstalk
      # @param [Hash] campaign: campaign detail to update on bidstalk
      # @return [Hash] Campaign detail if successful
      def update(campaign)
        campaign_id = campaign.delete(:campaign_id)
        item post(body: campaign, path: "#{ENDPOINT}/update/#{campaign_id}", content_type: APPLICATION_JSON)
      end

      # Pause campaign on bidstalk
      # @param [Integer] campaign_id 
      # @return [Hash] {campaign_id: id, campaign_title: title, campaign_status: status}
      def pause(campaign_id)
        item post(body: {status: 'PAUSED'}, path: "#{ENDPOINT}/update/#{campaign_id}", content_type: APPLICATION_JSON)
      end

      # Resume campaign on bidstalk
      # @param [Integer] campaign_id
      # @return [Hash] {campaign_id: id, campaign_title: title, campaign_status: status}
      def resume(campaign_id)
        item post(body: {status: 'RUNNING'}, path: "#{ENDPOINT}/update/#{campaign_id}", content_type: APPLICATION_JSON)
      end

      # Delete a campaign
      # @param [Integer] campaign_id
      # @return [Hash] {campaign_id: id, campaign_title: title, campaign_status: status}
      def delete(campaign_id)
        items post(path: "#{ENDPOINT}/delete/#{campaign_id}", content_type: APPLICATION_JSON)
      end
    end
  end
end