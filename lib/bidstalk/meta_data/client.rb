require 'bidstalk/client'

module Bidstalk
  module MetaData
    class Client < Bidstalk::Client

      def initialize(name, path=nil)
        @name = name
        @endpoint = "/#{path || @name}"
      end

      # Get meta data list from bidstalk
      # @return [Array] Meta data list
      def list
        items get, @name
      end
    end
  end
end