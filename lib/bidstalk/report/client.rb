require 'bidstalk/client'

module Bidstalk
  module Report
    class Client < Bidstalk::Client

      def initialize(params = {})
        @name = 'reports'
      end

      # Get report list from bidstalk
      # params [hash] filter
      #    "filter" : {
      #     "campaigns" : [11111,2222],
      #     "creatives" : [1111,2222],
      #     "countries" : [113,114],
      #     "os" : [11,12],
      #     "carriers" : [11,12],
      #     "manufacturers" : [11,12],
      #     "exchanges" : [11,12],
      #     "apps" : [“aaaaaa”, “bbbbbb”]
      # }
      # @return [Array] campaigns list
      def lists(filter = {}, start_date, end_date)
        report_params = {
          filter: filter,
          order_by: {
            impressions: "asc",
            clicks: "asc"
          },
          start_date: start_date,
          end_date: end_date
        }
        items post({report_params: report_params, path: 'advertiser/reports', content_type: APPLICATION_JSON }), @name
      end
    end
  end
end