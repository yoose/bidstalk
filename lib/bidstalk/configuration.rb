module Bidstalk
  class Configuration
    attr_accessor :api_key, :username

    def initialize
      @api_key = ''
      @username = ''
    end
  end
end