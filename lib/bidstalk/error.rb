module Bidstalk
  Error = Class.new(StandardError)

  ResourceNotFoundError = Class.new(Error)
  InvalidTokenError = Class.new(Error)
  InternalServerError = Class.new(Error)
  UnknownError = Class.new(Error)
  InvalidArgumentError = Class.new(Error)
  TimeoutError = Class.new(Error)
end