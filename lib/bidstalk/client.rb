require 'faraday'
require 'json'
require 'active_support/core_ext/string'

module Bidstalk
  class Client
    URL_PREFIX = 'http://demandapi.bidstalk.com'
    CONTENT_TYPES = [
        APPLICATION_JSON = 'application/json',
        MULTIPART_FORM_DATA = 'multipart/form-data',
        APPLICATION_FORM_URLENCODED = 'application/x-www-form-urlencoded',
    ]

    def api_key
      @api_key ||= Bidstalk.configuration.api_key
    end

    def username
      @username ||= Bidstalk.configuration.username
    end

    def token
      @token ||= auth
    end

    # @return [Faraday::Connection]
    def connection
      @connection ||= Faraday.new(url: URL_PREFIX) do |faraday|
        faraday.request  :multipart
        faraday.request  :url_encoded
        faraday.adapter Faraday.default_adapter
      end
    end


    # Send a POST request to Bidstalk API
    # @param [String] url
    # @param [Hash] params
    # @param [CONTENT_TYPES] content_type
    # @return [Hash] response body as hash
    def post(opts={})
      handle_connection do
        connection.post { |req| prepare_request req, opts }
      end
    end

    private

    def auth
      auth_url = '/advertiser/auth'

      res = handle_connection do
        connection.post do |req|
          req.url auth_url
          req.headers['Content-Type'] = APPLICATION_JSON
          req.body = {api_key: api_key, email: username}.to_json
        end
      end

      res[:data][:token] if res[:status] == 'success'
    end

    def prepare_request(req, opts={})
      body = { token: '', data: {} }
      req.url opts[:path].to_s
      req.headers['Content-Type'] = opts[:content_type] if opts[:content_type].present?
      body[:token] = token if token.present?
      body[:data][@name.to_sym] = opts[:body]
      body = body.merge(opts[:report_params]) if opts[:report_params].present?
      body = body.to_json if opts[:content_type] == APPLICATION_JSON
      req.body = body if body.present?
    end

    def handle_connection
      res = begin
        yield
      rescue Faraday::TimeoutError => e
        raise TimeoutError, e
      end

      handle_response res
    end

    def handle_response(res)
      result = res.body.nil? ? [] : json_result(res.body)
      if res.status==200 && result[:status] == 'success' 
        result
      elsif [1001, 1002, 1003, 1007, 1900, 1901].include?(result[:status_code])
        raise InvalidTokenError, 'Unauthorized'
      elsif result[:status] == 'failed' && result[:messages].present?
        raise InvalidArgumentError, result[:messages]
      else
        raise UnknownError, 'Unknown Error'
      end

    end

    def json_result(body)
      JSON.parse body, symbolize_names: true
    end

    def item(res)
      res.include?(@name.to_sym) ? res[@name.to_sym] : res
    end

    def items(res, attr_name=nil)
      attr_name =  attr_name.nil? ?  @name.pluralize.to_sym : attr_name.to_sym
      res.include?(attr_name) ? res[attr_name] : res
    end
  end
end