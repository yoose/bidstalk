require 'bidstalk/version'
require 'bidstalk/error'
require 'bidstalk/configuration'
require 'bidstalk/campaign/client'
require 'bidstalk/creative/client'
require 'bidstalk/meta_data/client'
require 'bidstalk/report/client'

module Bidstalk
  class << self
    attr_writer :configuration

    def configuration
      @configuration ||= Configuration.new
    end

    def reset
      @configuration = Configuration.new
    end

    def configure
      yield(configuration)
    end
  end
end
