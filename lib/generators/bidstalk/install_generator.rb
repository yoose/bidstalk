require 'rails/generators/base'

module Bidstalk
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path('../../templates', __FILE__)
      desc 'Creates a Bidstalk configuration and copy locale files to your application.'
      def copy_initializer
        template 'bidstalk.rb', 'config/initializers/bidstalk.rb'
      end
      def show_readme
        readme 'README' if behavior == :invoke
      end
      def rails_4?
        Rails::VERSION::MAJOR == 4
      end
    end
  end
end